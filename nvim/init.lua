-- init.lua
vim.g.mapleader = " "
-- Lazy package manager
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup("plugins")
require("lualine").setup()

vim.keymap.set('n', '<leader>`', '<cmd>Lazy sync<cr>', { noremap = true, silent = true })


-- Global settings
vim.cmd [[colorscheme tokyonight]]

-- Enable line numbers and relative line numbers
vim.wo.number = true
vim.wo.relativenumber = true

-- -- Enables termguicolors
vim.o.termguicolors = true

-- Enables system clipboard
vim.o.clipboard = "unnamedplus"

-- Enables splits
vim.o.splitright = true
vim.o.splitbelow = true

vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true

--Keymaps
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

-- File explorer
vim.keymap.set("n", "<leader>ef", "<cmd>Explore<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>ev", "<cmd>Rexplore<cr>", { noremap = true, silent = true })
