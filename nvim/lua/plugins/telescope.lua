return {
	'nvim-telescope/telescope.nvim',
	version = false,
	dependencies = {
		'nvim-lua/plenary.nvim'
	},
	cmd = "Telescope",
	keys = {
		{ "<leader>ff", "<cmd>Telescope find_files theme=dropdown<CR>", desc = "File search" },
		{ "<leader>fg", "<cmd>Telescope live_grep theme=dropdown<CR>",  desc = "Grep" },
		{ "<leader>fb", "<cmd>Telescope buffers theme=dropdown<CR>",    desc = "Current buffers" },
		{ "<leader>fx", "<cmd>Telescope git_files theme=dropdown<CR>",  desc = "Git file search" }
	},
	config = function()
		require("telescope").setup({
			defaults = {
				layout_config = {
					vertical = { width = 0.8 },
				},
			},
		})
	end,
}
